#!/bin/bash

git svn fetch
while [ $? -eq 1 ]; do
    echo "Failed sleeping for 5s"
    sleep 5
    git svn fetch
done
